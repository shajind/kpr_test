const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    username: {
        required: false,
        type: String
    },
    email:{
        required: true,
        type: String
    },
    password:{
        required: false,
        type: String
    },
    phoneNo:{
        required: true,
        type: String
    },
    age: {
        required: false,
        type: Number
    }
})

module.exports = mongoose.model('user', dataSchema)