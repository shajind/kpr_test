const router = this;
const check = require('validator');


exports.assignRoutes = function (app) {
    app.post('/api/create', router.create);
    app.get('/api/findAll', router.findAll);
    app.get("/api/findById", router.findById);
    app.put("/api/update/:id",router.update);
}

let missingString = (r1, f1) =>
    ((typeof r1[f1] === 'undefined') || (typeof r1[f1] !== 'string') || (r1[f1].trim().length === 0)) ? true : false;

const Model = require('./model/model');

exports.create = async (req, res) => {
    let bBody = req.body;
    let validation = [];
    if ((missingString(bBody, 'email')) || (!check.isEmail(bBody.email))) validation.push("email");
    if ((missingString(bBody, 'phoneNo')) || (bBody.phoneNo.length != 10)) validation.push("phoneNo");
    if (missingString(bBody, 'password')) validation.push("password");
    if (missingString(bBody, 'conformPassword')) validation.push("conformPassword");
    if (bBody.password != bBody.conformPassword) validation.push("password is not match");
    if (validation.length != 0) {
        res.status(400).json({ message: validation });
        return;
    }
    const data = new Model(bBody);
    try {
        const dataToSave = await data.save();
        res.status(200).json(dataToSave)
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
}


exports.findAll = async (req, res) => {
    try {
        const data = await Model.find();
        res.json(data)
    }
    catch (error) {
        res.status(500).json({ message: error.message })
    }
}


exports.findById = async (req, res) => {
    let bParam = req.query;
    let validation = [];
    if (missingString(bParam, 'id')) validation.push("id");
    if (validation.length != 0) {
        res.status(400).json({ message: validation });
        return;
    }
    try {
        const data = await Model.findById(bParam.id);
        res.json(data)
    }
    catch (error) {
        res.status(500).json({ message: error.message })
    }
}



exports.update = async (req, res) => {
    let bParam = req.params;
    let validation = [];
    if (missingString(bParam, 'id')) validation.push("id");
    if (validation.length != 0) {
        res.status(400).json({ message: validation });
        return;
    }
    try {
        const id = bParam.id;
        const updatedData = req.body;
        const options = { new: true };
        const result = await Model.findByIdAndUpdate(
            id, updatedData, options
        )

        res.send(result)
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
}