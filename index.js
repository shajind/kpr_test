const express = require('express');
const mongoose = require('mongoose');
const mongoString = 'mongodb://localhost:27017/?readPreference=primary&directConnection=true&ssl=false';
var bodyparser = require('body-parser');
mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log(error)
})





database.once('connected', () => {
    console.log('Database Connected');
})
const app = express();


app.use('/api', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,access-token,X-Key,group_id');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
})
app.use(bodyparser.json({ limit: '10mb' }));

const routes = require('./routes');
routes.assignRoutes(app);
app.use(express.json());

app.listen(7071, () => {
    console.log(`Server Started at ${7071}`)
})